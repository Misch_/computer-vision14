\documentclass{paper}

%\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{color}
\usepackage{here}
\usepackage{todonotes}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{hyperref}


% load package with ``framed'' and ``numbered'' option.
%\usepackage[framed,numbered,autolinebreaks,useliterate]{mcode}

% something NOT relevant to the usage of the package.
\setlength{\parindent}{0pt}
\setlength{\parskip}{18pt}






\usepackage[latin1]{inputenc} 
\usepackage[T1]{fontenc} 

\usepackage{listings} 
\lstset{% 
   language=Matlab, 
   basicstyle=\small\ttfamily, 
} 



\title{Computer Vision, Assignment 1\\Photometric stereo}
\date{28.10.2014}


\author{Mich\`ele Wyss\\10-104-123}

% //////////////////////////////////////////////////


\begin{document}



\maketitle


% Add figures:
%\begin{figure}[t]
%%\begin{center}
%\quad\quad   \includegraphics[width=1\linewidth]{ass2}
%%\end{center}
%
%\label{fig:performance}
%\end{figure}

\section{Calibration (35 points)}
In order to calculate the light source directions from the given pictures of a shiny chrome sphere, I performed the following steps:
\paragraph{Estimate sphere radius and center:} This step was implemented using the given mask of the sphere (Figure \ref{fig:sphereMask}). I used the straight forward approach of finding the largest and smallest $x$ and $y$ pixel coordinates where the mask values were non-zero. The center can then easily be found by taking the mean of the found values, and the radius by calculating the difference between a minimum or maximum point and the center since we assume an orthographic projection of the sphere.

\begin{figure}[h]
\begin{center}
\quad\quad   \includegraphics[width=0.5\textwidth]{figures/sphereMask}
\end{center}
\caption{Mask of the sphere -- used to estimate center and radius of the object during the initial calibration.}
\label{fig:sphereMask}
\end{figure}

\paragraph{Find the highlights:} As a next step, it is necessary to find the highlights in the given 12 pictures of the sphere. 
My first attempt was to simply take the pixel with the maximum value in the image. 
This was not exactly perfect as there are usually multiple pixels that have the same (maximum) value. 
Therefore, I used a similar approach to the one from above, where I simply used the values with the maximum values as a mask. 
See Figure \ref{fig:highlights} for some examples.

\begin{figure}[ht]
\centering
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/highlight1}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/highlight2}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/highlight3}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/highlight4}
\end{subfigure}
\caption{Examples of found highlights. It is always the average position of all the pixels containing the maximum value.}
\label{fig:highlights}
\end{figure}

\paragraph{Estimate the normals:} Given the center and radius of the sphere, calculating the normal vectors at each pixel is straight forward. 
Under the assumption of an orthographic projection, we can directly calculate the 3D position for each pixel $(x,y)$. 
By the definition of a sphere, it is given by
$$(X,Y,Z) = (x,y,\sqrt{r^2-x^2-y^2}),$$
where $r$ is the radius of the sphere. 
The normal vector N at each point $(x,y,z)$ is simply the point itself, normalized to unit length:
$$N = \frac{1}{\|(x,y,z)\|} \cdot (x,y,z)^T.$$

\paragraph{Estimate the light directions:} Given the so far calculated things and under the assumption of an orthographic projection (i.e.\ we know that the direction of the camera from any point on the object is R = (0, 0,-1)), we can calculate the light direction $L$ for each of the 12 given images as
$$L = 2(N \cdot R)N - R. $$


For the given images and the just described procedure, this yields the following 12 light source directions:
\begin{align}
\mathbf{L}= \left[ \begin{array}{cccccccccccccc}
   0.49 & 0.24 & -0.04 & -0.10 & -0.33 & -0.12 & 0.28 & 0.10 & 0.20 & 0.08 & 0.13 & -0.15 \\
  -0.47 & -0.14 & -0.18 & -0.44 & -0.51 & -0.57 & -0.44 & -0.43 & -0.34 & -0.34 & -0.05 & -0.37 \\
  -0.73 & -0.96 & -0.98 & -0.89 & -0.80 & -0.81 & -0.86 & -0.90 & -0.92 & -0.94 & -0.99 & -0.92
\end{array} \right] \nonumber
\end{align}

\section{Computing Surface Normals and Grey Albedo (30 points)}
Givern the estimated directions of the light sources, we can use the following formula to describe the observed intensity:
$$I(x,y) = a(x,y) (n(x,y) \cdot L).$$
In the above formula, $I(x,y)$ denotes the pixel value at pixel $(x,y)$, $a$ is the albedo, $n$ the surface normal and $L$ the direction of the light source. The known values are $I(x,y)$ and $L$. The albedo $a(x,y)$ and the surface normal $n(x,y)$ are unknown. We can write this as a system of equations
$$I = vL,$$
where $v$ contains the normal vectors with length $a$ (albedo). To be more precise, the matrix $I$ is a $(\#pixels \times 12)$-matrix containing the gray scale pixel values of all the 12 given input images (under the same lighting conditions as the chrome sphere from which we estimated the light directions). 
$L$ is a $(3 \times 12)$-matrix where the i$^{\text{th}}$ column contains the (x,y,z)-coordinates of the estimated light direction for the i$^{\text{th}}$ image. 
The unknown matrix $v$ is of size $(\#pixels \times 3)$ where each row contains the (x,y,z)-coordinates of the estimated (non-unit!) normal at the corresponding pixel. 

We can solve this system of linear equations using the matlab-function \lstinline{mrdivide(I,L)} 
which will give us the least squares solution (for more details about that, see e.g.\ the \href{http://www.mathworks.ch/help/matlab/ref/mrdivide.html}{Matlab Documentation}) for $v$. 

From $v$ we can recover the albedo $a$ (size $\#pixels \times 1$) by taking the row-wise norm of $v$. The row-wise normalized $v$ is then exactly the normals $n$ (size $\#pixels \times 3$). Some examples for the recovered gray albedo can be seen in Figure \ref{fig:albedoGray}.


\begin{figure}[ht]
\centering
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/albedoBuddhaGray}
	\caption*{Buddha data set}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/albedoHorseGray}
	\caption*{Horse data set}
\end{subfigure}

\vspace{2mm}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/albedoCatGray}
	\caption*{Cat data set}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/albedoOwlGray}
	\caption*{Owl data set}
\end{subfigure}
\caption{Some examples of the recovered gray albedo maps.}
\label{fig:albedoGray}
\end{figure}


With the given normal, the albedo for each color channel can then be computed by a simple (pointwise) division:
\begin{align*}
a_R(x,y) &= \frac{I_R(x,y)}{(n(x,y)\cdot L)} \\
a_G(x,y) &= \frac{I_G(x,y)}{(n(x,y)\cdot L)} \\
a_B(x,y) &= \frac{I_B(x,y)}{(n(x,y)\cdot L)}
\end{align*}
This yields actually 12 values per channel because $L$ contains $12$ estimated light sources and we have 12 different input images $I$. As can be seen in some examples in Figure \ref{fig:albedoMeanVsMedian}, each of the $12$ recovered albedo maps contain some outlier artifacts at different positions, due to the different regions that are properly ``seen'' by the different light sources. After trying out few different solutions and discussing them with other students, I decided to take the median of the 12 values as the final albedo because the median is robust to outliers.

\begin{figure}[ht]
\centering
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/albedoBuddhaFirst}
	\caption*{1$^{\text{st}}$ recovered albedo}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/albedoBuddhaFifth}
	\caption*{5$^{\text{th}}$ recovered albedo}
\end{subfigure}

\vspace{2mm}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/albedoBuddhaMean}
	\caption*{Mean at each pixel}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/albedoBuddhaMedian}
	\caption*{Median at each pixel}
\end{subfigure}
\caption{Examples of found highlights. It is always the average position of all the pixels containing the maximum value.}
\label{fig:albedoMeanVsMedian}
\end{figure}

The recovered RGB-albedo and normal vectors for all data sets can be seen in Figures \ref{fig:albedoAllDatasets} and \ref{fig:recoveredNormals}.


\begin{figure}[ht]
\centering
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/albedoSphereRGB}
	\caption*{Sphere}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/albedoBuddhaRGB}
	\caption*{Buddha}
\end{subfigure}

\vspace{2mm}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/albedoCatRGB}
	\caption*{Cat}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/albedoOwlRGB}
	\caption*{Owl}
\end{subfigure}

\vspace{2mm}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/albedoHorseRGB}
	\caption*{Horse}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/albedoRockRGB}
	\caption*{Rock}
\end{subfigure}
\caption{The recovered RGB albedos for all the data sets.}
\label{fig:albedoAllDatasets}
\end{figure}

\begin{figure}[ht]
\centering
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/normalsSphere}
	\caption*{Sphere}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/normalsBuddha}
	\caption*{Buddha}
\end{subfigure}

\vspace{2mm}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/normalsCat}
	\caption*{Cat}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/normalsOwl}
	\caption*{Owl}
\end{subfigure}

\vspace{2mm}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/normalsHorse}
	\caption*{Horse}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/normalsRock}
	\caption*{Rock}
\end{subfigure}
\caption{The recovered normal vectors. The absolute values of the (x,y,z)-coordinates are visualized as RGB colors.}
\label{fig:recoveredNormals}
\end{figure}

\section{Surface Fitting (35 points)}
As described in the exercise sheet, we can express our surface as a parametrized surface
$$S(x,y) = (x,y,z(x,y))^T$$
and the tangent vector $t_x$ and $t_y$ can be given by taking the derivatives of $S$ with respect to $x$ and $y$, respectively:
\begin{align*}
 t_x &= \left(1, 0, \frac{\partial z}{\partial x}\right)^T \\
 t_y &= \left(1, 0, \frac{\partial z}{\partial y}\right)^T
\end{align*}
and the surface normal $n$ can then be given as the cross product of the two tangent vectors,
$$n(x,y) = \frac{(z_x,z_y,-1)^T}{\|(z_x,z_y,-1)\|}.$$

However, we estimated these normals already and will denote its components as
$$n(x,y) = (n_x,n_y,n_z)^T.$$

To find the unknown depth values, we use the constraints that 
\begin{align*}
 t_x(x,y) \cdot n(x,y) &= 0 \\
 t_y(x,y) \cdot n(x,y) &= 0.
\end{align*}
Written out, this yields the following contraints:
\begin{align*}
 n_x + n_z ~z_x = 0 &\Longleftrightarrow  - \frac{n_x}{n_z} = z_x = z(x+1,y)- z(x,y)\\
 n_y + n_z ~z_y = 0 &\Longleftrightarrow  - \frac{n_y}{n_z} = z_y = z(x,y+1)- z(x,y).
\end{align*}
Here, we used forward differences to approximate the gradients $z_x$ and $z_y$. This can directly be reformulated as a vector-matrix multiplication
$$A\mathbf{z} = \mathbf{v},$$
where $\mathbf{v}$ contains the estimated values from the normals,
\begin{align}
\mathbf{v}= \left[ \begin{array}{c}
   -\frac{n_{x,1}}{n_{z,1}}  \\
   -\frac{n_{x,2}}{n_{z,2}}  \\
   -\frac{n_{x,3}}{n_{z,3}}  \\
   \vdots \\
   -\frac{n_{x,\#pixels}}{n_{z,\#pixels}}  \\ \\ \hline \\
   -\frac{n_{y,1}}{n_{z,1}}  \\
   -\frac{n_{y,2}}{n_{z,2}}  \\
   -\frac{n_{y,3}}{n_{z,3}}  \\
   \vdots \\
   -\frac{n_{y,\#pixels}}{n_{z,\#pixels}}
\end{array} \right] \nonumber
\end{align}
And A is a convolution matrix representing the forward difference derivatives with respect to $x$ and $y$:

\begin{align}
A = \left[ \begin{array}{cccccc}
  1 & -1 & & & & \\
  & 1 & -1 & & & \\
  & & \ddots & \ddots & & \\
  & & & 1 & -1 & \\
  & & & & 1 & -1 \\ \\ \hline \\
  -1 & \hdots & 1 & & & \\
  & -1 & \hdots & 1 & & \\
  & & \ddots & \ddots &  & \\
  & & -1 & \hdots  & 1 &  \\
  & & & -1 & \hdots & 1 
\end{array} \right] \nonumber
\end{align}

In matlab, this sparse system of equations can be solved (least squares) using the backslash operator: \lstinline{z = A\v}. 
Sadly, I didn't manage to correctly implement the algorithm correctly -- I assume that the sparse matrix is not correctly set. In Figure \ref{fig:recoveredDepthMaps} you can see my resulting depth maps.


\begin{figure}[ht]
\centering
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/depthSphere}
	\caption*{Sphere}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/depthBuddha}
	\caption*{Buddha}
\end{subfigure}

\vspace{2mm}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/depthCat}
	\caption*{Cat}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/depthOwl}
	\caption*{Owl}
\end{subfigure}

\vspace{2mm}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/depthHorse}
	\caption*{Horse}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{figures/depthRock}
	\caption*{Rock}
\end{subfigure}
\caption{The recovered depth maps. Unfortunately, something seems to be wrong with my setup of the matrix. Therefore, the results are not very good.}
\label{fig:recoveredDepthMaps}
\end{figure}

 \end{document}