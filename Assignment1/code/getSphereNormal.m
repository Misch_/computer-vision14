function [n] = getSphereNormal(x, y ,r, c)

% center coordinates at sphere center
xCoord = x - c(1);
yCoord = y - c(2);

% We know that x^2 + y^2 + z^2 = r^2
% ==> Use this to calculate z-coordinate
% (take negative because camera lies in negative position)
z = -sqrt(r^2 - (xCoord)^2 - (yCoord)^2);
n = [xCoord yCoord z];
n = n/norm(n);
