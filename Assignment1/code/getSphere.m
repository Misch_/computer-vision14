function [c r] = getSphere(mask)
% M is a row vector containing the maximum values of the columns of the mask.
  % M = [0 0 ... 0 0 1 ... 1 0 0 ... 0 0]
  M = max(mask);
  
  % find(M,1,'first') is the first index that contains a non-zero value
  minX = find(M,1,'first');
  maxX = find(M,1,'last');
  centerX = (maxX+minX)/2;
  
  % As above, but consider rows of the mask instead of columns (transpose
  % mask)
  M = max(mask');
  minY = find(M,1,'first');
  maxY = find(M,1,'last');
  centerY = (maxY+minY)/2;

  c = [centerX centerY];
  r = maxY - centerY;