function [depth] = getDepthFromNormals(n, mask)
  % [depth] = getDepthFromNormals(n, mask)
  %
  % Input:
  %    n is an [N, M, 3] matrix of surface normals (or zeros
  %      for not available normal).
  %    mask logical [N,M] matrix which is true for pixels
  %      at which the object is present.
  % Output
  %    depth an [N,M] matrix providing depths which are
  %          orthogonal to the normals n (in the least
  %          squares sense).
  %
  
  
  imsize = size(mask); % [340 512]
  reshaped_mask = reshape(mask,[],1);
%   dxMat = convmtx2([1 -1], 1,prod(imsize));
  dxMat = convmtx2([1 -1], imsize);
  dxMat = dxMat(1:prod(imsize),:);
  dxMat(~reshaped_mask,:) = 0;
  
  dyMat = convmtx2([1;-1], imsize);
  dyMat = dyMat(1:prod(imsize),:);
  dyMat(~reshaped_mask,:) = 0;

  % Contraint to fix one depth value
  A = zeros(1,prod(imsize));
  A(end) = 1;
  A = sparse(A);
  
  % This matrix is probably not correct...
  DxDy = [dxMat;dyMat;A];
  
  
  reshaped = reshape(n,[],3);
  
  xTangent = reshaped(:,1)./reshaped(:,3);
  yTangent = reshaped(:,2)./reshaped(:,3);
  solutionVec = [xTangent;yTangent;0];
  solutionVec(~reshaped_mask,:) = 0;
  
 
  depth = DxDy\solutionVec;
  depth = reshape(depth,imsize);
  depth(~mask) = min(min(depth));
  