function [n, albedo] = fitReflectance(im, L)
  % [n, albedo] = fitReflectance(im, L)
  % 
  % Input:
  %   im - nPix x nDirChrome array of brightnesses,
  %   L  - 3 x nDirChrome array of light source directions.
  % Output:
  %   n - nPix x 3 array of surface normals, with n(k,1:3) = (nx, ny, nz)
  %       at the k-th pixel.
  %   albedo - nPix x 1 array of estimated albdedos

  % We know: For each pixel x, we have
  % im(x) = a(x)(n(x)*L)
   nLeastSquares = mrdivide(im,L);
   
   albedo = sqrt(sum(nLeastSquares.^2,2));
   n = normr(nLeastSquares);
  return;