function [L] = fitChromeSphere(chromeDir, nDir, chatty)
  % [L] = fitChromeSphere(chromeDir, nDir, chatty)
  % Input:
  %  chromeDir (string) -- directory containing chrome images.
  %  nDir -- number of different light source images.
  %  chatty -- true to show results images. 
  % Return:
  %  L is a 3 x nDir image of light source directions.

  
  % Since we are looking down the z-axis, the direction
  % of the light source from the surface should have
  % a negative z-component, i.e., the light sources
  % are behind the camera.
    
  if ~exist('chatty', 'var')
    chatty = false;
  end
    
  mask = imread([chromeDir, 'chrome.mask.png']);
  mask = mask(:,:,1) / 255.0;

  [center radius] = getSphere(mask);
  
  L = zeros(3,nDir); % size = 3 x nDir
  nDir = 12;
  for n=1:nDir
    fname = [chromeDir,'chrome.',num2str(n-1),'.png'];
    im = imread(fname);
    imData(:,:,n) = im(:,:,1);           % red channel
  end
  
  for n=1:nDir
    img = imData(:,:,n);
    [highlight] = getHighlight(img);
    
    normal = getSphereNormal(highlight(1),highlight(2),radius,center);
    R = [0 0 -1];
    lightDir = 2* dot(normal,R)*normal - R;
    L(:,n) = lightDir/norm(lightDir);
  end