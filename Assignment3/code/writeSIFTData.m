% Add path to vl_feat toolbox.
vl_feat_path = [matlabroot, '\vlfeat-0.9.19\toolbox\vl_setup'];
run(vl_feat_path);

fnames = dir(['../frames/*.png']);
fprintf('reading %d total files...\n', length(fnames));

for i = 1:length(fnames)
   img_filename = fnames(i).name;
   
    % load one frame
    % img_filename = 'frame_012.png';
    img = imread(['../frames/',img_filename]);

    img = single(rgb2gray(img));
    % imshow(img, [0 255]);

    % The matrix f has a column for each frame. A frame is a disk of center
    % f(1:2), scale f(3) and orientation f(4).
    [f, d] = vl_sift(img);

    descriptors = d';
    imname = img_filename;
    numfeats = length(f);   % number of detected features
    orients = f(4,:)';      % orientations of the patches
    positions = f(1:2,:)';  % positions of the patch centers
    scales = f(3,:)';       % scales of the patches

    % TODO: remove the .png from the filename! %
    sift_filename = ['../sift_values/',imname,'.mat'];
    save(sift_filename,'descriptors','imname','numfeats','orients','positions','scales');
end


% % Visualize Stuff: 
% figure(1); imshow(img, [0 255]);
% hold on;
% perm = randperm(size(f,2)) ;
% sel = perm(1:50) ;
% h1 = vl_plotframe(f(:,sel)) ;
% h2 = vl_plotframe(f(:,sel)) ;
% set(h1,'color','k','linewidth',3) ;
% set(h2,'color','y','linewidth',2) ;