video_filename = '../video/teenageWitch.avi'
vidObj = mmreader(video_filename);

frames_dir = '../frames/';
start_frame = 12;
frames = read(vidObj,[10 150]);

for i = 1:size(frames,4)
   frameNr = sprintf('%03d',i+start_frame-1);
   filename = [frames_dir,'frame_',frameNr,'.png'];
   imwrite(frames(:,:,:,i),filename);
end
