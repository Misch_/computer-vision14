%% Assignment  1.3.2 - Visualizing the vocabulary

%% Build vocabulary
framesdir = '../frames/';
siftdir = '../sift_values/';


% Get a list of all the .mat files in that directory.
% There is one .mat file per image.
fnames = dir([siftdir '/*.mat']);

fprintf('reading %d total files...\n', length(fnames));

% Loop through all the data files found and put the data together. The
% variable nImg is only there to keep track to which file the data
% correspond. This will be used later to display the image.
all_descriptors = [];
all_positions = [];
all_scales = [];
all_orients = [];
nImg = [];
for i=1:length(fnames)

    fprintf('reading frame %d of %d\n', i, length(fnames));
    
    % load the file
    fname = [siftdir '/' fnames(i).name];
    load(fname, 'descriptors','positions','orients','scales');
    
    all_descriptors = [all_descriptors; descriptors];
    all_positions = [all_positions; positions];
    all_scales = [all_scales; scales];
    all_orients = [all_orients; orients];
    nImg = [nImg; ones(size(descriptors,1),1) * i];
end

% get a large random sample of the sift descriptors
nOfSamples = 70000;
rand_idx = randsample(size(all_descriptors,1),nOfSamples);
all_descriptors = all_descriptors(rand_idx,:);
all_positions = all_positions(rand_idx,:);
all_scales = all_scales(rand_idx);
all_orients = all_orients(rand_idx);
nImg = nImg(rand_idx);

% build vocabulary by clustering (kmeans)
disp('K-means...');
[membership means rms] = kmeansML(1000,single(all_descriptors'));
disp('Done.');

%% Chose and visualize patches of a random word
rand1 = round(rand * 800);

members = find(membership==rand1);

for i = 1:min(length(members),25)
    idx = members(i);
    img_file =  fnames(nImg(idx)).name;
    img_file = img_file(1:end-4);
    
    im = rgb2gray(imread([framesdir,img_file]));
    patch = getPatchFromSIFTParameters(all_positions(idx,:), all_scales(idx), all_orients(idx), im);
    figure(1); subplot(5,5,i); imshow(patch); 
    
end
figure(1); suptitle(['Word number ',num2str(rand1)]);