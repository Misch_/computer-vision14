%% Assignment 1.3.4 - Region queries
% Assumption: The script "visualizeVocabulary" ran through before.
% run('visualizeVocabulary');
close all;
framesdir = '../frames/';
fnames = dir([framesdir '/*.png']); 

queryFrames = [20 100 55 120];

for queryFrame = 1:length(queryFrames)
    img = imread([framesdir,fnames(queryFrame).name]);
    load(['../sift_values/',fnames(queryFrame).name,'.mat']);

    % Compute histograms for all frames
    for i=1:length(fnames)
       img_membership = membership(nImg == i);
       histo(:,i) = histc(img_membership,1:length(means));
    end

    % Compute query histogram based on only the region
    img_membership = membership(nImg == queryFrames(queryFrame));
    img_positions = all_positions(img_membership,:);
    img_scales = all_scales(img_membership);
    img_orients = all_orients(img_membership);
    figure(3);
    [selected poly] = selectRegion(img,img_positions);
    polyPos = getPosition(poly);
    ref_histo = histc(img_membership(selected),1:length(means));
    
    % weight the region histogram with tf-idf
     
    % log-scaled term-frequency
    % tf = (ref_histo * 0.5)/max(ref_histo) + 0.5);
    tf = ref_histo;
    all_histo = histc(membership,1:length(means));
    weighted_histo = ( tf .* (ones(size(all_histo))*log(sum(all_histo))./max(all_histo,0.2)));
    weighted_norm = norm(weighted_histo);
    
    scores = zeros(1,size(histo,2));
    for i = 1:size(histo,2)
       scores(i) = dot(histo(:,i),weighted_histo) / (norm(histo(:,i)) * weighted_norm);
    end

    [sorted_scores indices] = sort(scores,'descend');
    bestMatchingFrames = indices(2:7);

    % Display the query frame and the found descriptors within the region
    frames = fnames(bestMatchingFrames);
    figure('name',fnames(queryFrame).name);
    subplot(2,4,1);
    imshow(imread([framesdir,fnames(queryFrame).name]));
    hold on;
    plot([polyPos(:,1); polyPos(1,1)], [polyPos(:,2); polyPos(1,2)],'y','LineWidth',2.5);
    title(['Query: ',fnames(queryFrame).name],'interpreter','none');
    
    subplot(2,4,2);
    imshow(imread([framesdir,fnames(queryFrame).name]));
    title('Descriptors','interpreter','none');
    displaySIFTPatches(img_positions(selected,:), img_scales(selected),img_orients(selected),img);
    
    % display 6 best matching frames
    for i = 1:length(frames)
        img_file = frames(i).name;
        img = imread([framesdir,img_file]);
        subplot(2,4,i+2); 
        imshow(img); 
        title(img_file,'interpreter','none');
    end
end