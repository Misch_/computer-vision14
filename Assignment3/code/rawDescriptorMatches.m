%% Assignment  1.3.1 - Raw descriptor matching

load twoFrameData.mat; 

% Let the user select a region
[selected poly] = selectRegion(im1,positions1);
polyPos = getPosition(poly);

% Get positions, scales, orients from selected SIFT-Features
positions1InRegion = positions1(selected,:);
scales1InRegion = scales1(selected);
orients1InRegion = orients1(selected);

% squared_distances is a matrix where entry A_ij contains the euclidean
% distance between descriptors1(:,i) and descriptors2(:,j).
squared_distances = distSqr(descriptors1(selected,:)',descriptors2');

% sort the distance values to find the 50 best matching sift-descriptors
[sorted, idx] = sort(squared_distances(:));
[rows cols] = ind2sub(size(squared_distances),idx(1:50));

% Visualize the matching features
figure(1);
subplot(1,2,1);

% Display 1st image and selected polygon
imshow(im1);
hold on;
plot([polyPos(:,1); polyPos(1,1)], [polyPos(:,2); polyPos(1,2)],'y','LineWidth',2.5);
% Display found descriptors (for debugging)
% displaySIFTPatches(positions1InRegion(rows,:), scales1InRegion(rows), orients1InRegion(rows), im1);


% The j-element of the matrix ( --> cols) always indicates which feature was chosen in
% the 2nd image
subplot(1,2,2);
imshow(im2);
displaySIFTPatches(positions2(cols,:), scales2(cols), orients2(cols), im2); 