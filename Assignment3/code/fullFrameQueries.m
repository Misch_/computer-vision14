%% Assignment 1.3.3 - Full frame queries
% Assumption: The script "visualizeVocabulary" ran through before.
% run('visualizeVocabulary');

% Calculate bag-of-words histogram for each frame
framesdir = '../frames/';
fnames = dir([framesdir '/*.png']); 

for i=1:length(fnames)
   img_membership = membership(nImg == i);
   histo(:,i) = histc(img_membership,1:length(means));
end

% set the query frames
queryImage = [1 139 28 50 ];

for query = 1:length(queryImage)
    % Compute scores for all the frames using normalized dot product.
    % Maximum score = 1
    ref_histo = histo(:,queryImage(query));
    ref_norm = norm(ref_histo);

    scores = zeros(1,size(histo,2));
    for i = 1:size(histo,2)
       scores(i) = dot(histo(:,i),ref_histo) / (norm(histo(:,i)) * ref_norm);
    end

    [sorted_scores indices] = sort(scores,'descend');
    bestMatchingFrames = indices(2:6);

    % display 5 best matching frames
    frames = fnames(bestMatchingFrames);
    figure(query);
    subplot(2,3,1);
    imshow(imread([framesdir,fnames(queryImage(query)).name]));
    title(['Query: ',fnames(queryImage(query)).name],'interpreter','none');
    for i = 1:length(frames)
        img_file = frames(i).name;
        img = imread([framesdir,img_file]);
        subplot(2,3,i+1); imshow(img);
        title({img_file,[' Score: ',num2str(sorted_scores(i))]},'interpreter','none');
    end
end