\documentclass{paper}

%\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{color}
\usepackage{here}
\usepackage{todonotes}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{hyperref}


% load package with ``framed'' and ``numbered'' option.
%\usepackage[framed,numbered,autolinebreaks,useliterate]{mcode}

% something NOT relevant to the usage of the package.
\setlength{\parindent}{0pt}
\setlength{\parskip}{18pt}






\usepackage[latin1]{inputenc} 
\usepackage[T1]{fontenc} 

\usepackage{listings} 
\lstset{% 
   language=Matlab, 
   basicstyle=\small\ttfamily, 
} 



\title{Computer Vision, Assignment 3\\Video search with bags of visual words}
\date{28.10.2014}


\author{Mich\`ele Wyss\\10-104-123}

% //////////////////////////////////////////////////
\graphicspath{{imgs/}}

\begin{document}



\maketitle


% Add figures:
%\begin{figure}[t]
%%\begin{center}
%\quad\quad   \includegraphics[width=1\linewidth]{ass2}
%%\end{center}
%
%\label{fig:performance}
%\end{figure}

\section{Data (25 points)}
I chose a video sequence of the series ``The Teenage Witch.'' The first step was to simply download the short video from \url{www.youtube.com} and save it as an {\tt .avi}-file. I wrote a little \textsc{Matlab} script {\tt videoToFrames.m} to extract some of the frames to images. You can see two example frames in Figure \ref{fig:exampleFrames}. Also, I wrote a scritp {\tt writeSIFTData} that computes and stores the SIFT-features of all frames to {\tt .mat}-files.

\begin{figure}[ht]
\centering
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{frame_020}
\end{subfigure}
\begin{subfigure}[h]{0.44\textwidth}
	\centering
	\includegraphics[width=\textwidth]{frame_089}
\end{subfigure}
\caption{Two examples of the extracted video frames.}
\label{fig:exampleFrames}
\end{figure}

\section{Raw descriptor matching (15 points)}
In my implementation of raw descriptor matching (script {\tt rawDescriptorMatches.m}), I first load the test data and then start by asking the user to select a region. For this, I used the provided function {\tt selectRegion}. I adapted the function a bit such that it's easier to retrieve the positions of the polygon because I will need those later in order to draw it.

The next step is to get the values (positions, scales, orients) from the SIFT-features that are inside the selected region. This is done by simply using the indices that the {\tt selectRegion} returned. 

A next main step is to compute the squared distances between the SIFT-descriptors:
\begin{verbatim}
 squared_distances = (distSqr(descriptors1(selected,:)',descriptors2'));
\end{verbatim}
In the above code-snippet, \lstinline{descriptors1} 
denotes the descriptors of the 1$^\text{st}$ image from which only the ones are used that lie inside the selected region. 
The variable \lstinline{descriptors2} 
denotes the descriptors of the 2$^\text{nd}$ image. The result \lstinline{squared_distances} 
is a matrix where entry (ij) contains the euclidean distance between the $i^\text{th}$ descriptor of the first and the $j^\text{th}$ of the second image (\lstinline{descriptors1(:,i)} and \lstinline{descriptors2(:,j)}, respectively).

Using \textsc{Matlab}'s {\tt sort}-function, I retrieved the 50 best matching SIFT-descriptors and then visualized the patches using the provided function {\tt displaySIFTPatches}. Some results can be seen in Figure \ref{fig:rawDescriptorMatchesExamples}.

\begin{figure}[ht]
\centering
\begin{subfigure}[h]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{rawMatchExample1}
\end{subfigure}
\begin{subfigure}[h]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{rawMatchExample2}
\end{subfigure}
\caption{Some results of the raw descriptor matching. The yellow polygon indicates the selected region, the blue squares denote the 50 best matching patches.}
\label{fig:rawDescriptorMatchesExamples}
\end{figure}

\section{Visualizing the vocabulary}
After loading the SIFT-features of all frames and concatenating them to single huge vectors \lstinline{all_descriptors, all_positions, all_scales, all_orients}, 
I select a hardcoded value of $70'000$ random descriptors to which the k-means algorithm with $k = 1'000$ is applied. I noticed that the algorithm yields a warning ``Warning: Re-running kmeans due to empty cluster'' which indicates that my value of $k$ is maybe too large. However, the results of the matching words do not really lead imply that there is some overfitting. Some examples of patches that match the same word can be seen in Figure \ref{fig:visualizedVocabulary}

\begin{figure}[ht]
\centering
\begin{subfigure}[h]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{vocabularyWord1}
\end{subfigure}
\begin{subfigure}[h]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{vocabularyWord2}
\end{subfigure}
\caption{Examples for two words and 25 matching patches.}
\label{fig:visualizedVocabulary}
\end{figure}

\section{Full frame queries (20 points)}
\subsection{Implementation}
Based on the previously calculated vocabulary, the first step is to calculate the bag-of-words histograms for all frames. As proposed, I implemented this using the \textsc{Matlab}-function {\tt histc}. The arguments that I gave to the function were for every frame the vector \lstinline{img_membership} 
containing the assignments of the descriptors of one image to the clusters, and the vector containing the numbers from 1 to $k$ where $k$ is the total number of clusters as mentioned in the previous section.

Once the histograms are built, the matching scores between each of them and the ``reference'' histogram (that is, the histogram of the query frame) are computed using the normalized dot product:
$$\text{score}(v_1,v_{\text{ref}}) \quad = \quad \frac{v_1 \cdot v_{\text{ref}}}{\|v_1\|_2 \cdot \|v_{\text{ref}}\|_2},$$
where $v_1$ and $v_{\text{ref}}$ are two $(k \times 1)$-vectors, each of them representing a bag-of-words histogram.

\subsection{Results}
Using the {\tt sort}-function, the best-matching scores and their indices within the vector are retrieved from which the corresponding frames can be found. I don't show the first (``best'') corresponding frame since it will typically be the query-frame itself. However, a nicer approach would have been to not even compute this score. Some results can be seen in Figures \ref{fig:fullFrameQueries1} and \ref{fig:fullFrameQueries2}.
\begin{figure}[ht]
\centering
\begin{subfigure}[h]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{fullFrameQuery1}
\end{subfigure}
\begin{subfigure}[h]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{fullFrameQuery2}
\end{subfigure}
\caption{2 examples of a query frame and the 5 best matching ones.}
\label{fig:fullFrameQueries1}
\end{figure}

\begin{figure}[ht]
\centering
\begin{subfigure}[h]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{fullFrameQuery3}
\end{subfigure}
\begin{subfigure}[h]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{fullFrameQuery4}
\end{subfigure}
\caption{Another 2 examples of a query frame and the 5 best matching ones.}
\label{fig:fullFrameQueries2}
\end{figure}

It is often the case that the best matching frames are very close in time to the query frame, except if there is a scene cut in between. This is a reasonable result since usually the scene does not change too much within a frame. A good example for this is the one from the top in Figure \ref{fig:fullFrameQueries1}. The query frame was frame 61 and the 5 best matching frames were 62, 64, 60, 59 and 67.

\section{Region queries (20 points)}
The region queries implementation is basically a combination of the previous tasks. First, the user has to select a query region from a (hardcoded) frame. This is done as in the first task (raw descriptor matching) with the function {\tt selectRegion}. The comparison of the scores is done in the very same way as in the section about full frame queries. The only difference is actually the computation of the reference histogram because there only the SIFT-features from withing the user selected region are considered. Since the region may possibly contain unmeaningful features that are very common in the whole descriptors set and therefore not an indicator of providing representative data for the chosen region, the histogram is weighted by the tf-idf weights \url{http://en.wikipedia.org/wiki/Tf%E2%80%93idf} 
that are commonly used, especially in the field of Natural Language Processing. 
Some results can be seen in Figures \ref{fig:regionQueries1} and \ref{fig:regionQueries2}.

\begin{figure}[ht]
\centering
\begin{subfigure}[h]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{regionQuery1}
\end{subfigure}
\begin{subfigure}[h]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{regionQuery2}
\end{subfigure}
\caption{Two examples of region queries including the 6 best matching frames.}
\label{fig:regionQueries1}
\end{figure}

\begin{figure}[ht]
\centering
\begin{subfigure}[h]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{regionQuery3}
\end{subfigure}
\begin{subfigure}[h]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{regionQuery4}
\end{subfigure}
\caption{Another two examples of region queries including the 6 best matching frames.}
\label{fig:regionQueries2}
\end{figure}
A failure case can be seen in the second example of Figure \ref{fig:regionQueries1}. The query region contains only very few descriptors, and they are well matched by some frames. However, there are other images included that would actually perfectly match exactly those two descriptors because the very same patch would be contained in the image.

A problem could be that the found frames match the queried ones quite well and at the same time there's less differences in the other frames because there's a big uniform region (the pink sweater) in the ``best matching'' frames. A different weighting of the reference histogram can help in this case (see comparison in Figure \ref{fig:compTFIDF}).
\begin{figure}[ht]
\centering
\begin{subfigure}[h]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{tf-idf1}
\end{subfigure}
\begin{subfigure}[h]{\textwidth}
	\centering
	\includegraphics[width=\textwidth]{tf-idf2}
\end{subfigure}
\caption{Comparison of differen tf-idf weighting. Top: Absolut term frequency. Bottom: log term frequency.}
\label{fig:compTFIDF}
\end{figure}
 \end{document}