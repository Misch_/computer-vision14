clear all
%close all
clc

maxSize = 500;
% We assume images have the same size
left = mean(single(imread('pics/left.jpg')),3);
[M N] = size(left);
ratio = max(M,N)/maxSize;
left = imresize(left,1/ratio);

right = mean(single(imread('pics/right.jpg')),3);
right = imresize(right,1/ratio);

[rows cols] = size(left);

figure(1)
clf;
subplot(1,2,1);
imagesc(left);
colormap(gray) 
title('Left Image');

subplot(1,2,2);
imagesc(right);
colormap(gray)
title('Right Image');

% Number of corresponding points.
numPoints = 8;
% Set to true to save the points in the file savedPoints.mat.
savePoints = false;
% Set to true to select new points even if a the file savedPoints.mat
% exists.
selectNewPoints  = false;

% Corresponding points in homogeneous coordinates.
leftPoints = zeros(3, numPoints);
rightPoints = zeros(3, numPoints);

if exist('savedPoints.mat','file') && ~selectNewPoints
    load('savedPoints.mat')
else
    uiwait(msgbox({'Select 8 corresponding point pairs:' '' '1. Chose one point on the left image' '2. Choose the corresponding one on the right image' ''},'Choose points'));
    for i=1:numPoints
        color = [log(i)/log(8) 1 mod(i,2)];
        [xL,yL] = ginput(1);
        hold on; plot(xL, yL, 'Color',color,'Marker','+', 'MarkerSize', 5, 'LineWidth', 3)
        leftPoints(:,i) = [xL yL 1];
        
        [xR, yR] = ginput(1);
        hold on; plot(xR, yR, 'Color',color,'Marker','+', 'MarkerSize', 5, 'LineWidth', 3)
        rightPoints(:,i) = [xR yR 1];
    end

    if savePoints
        save('savedPoints.mat','leftPoints','rightPoints');
    end
end


% Computing the fundamental matrix
F = eightPointsAlgorithm(leftPoints,rightPoints);



disp('Select a point in the left image. Press ESC to exit.');
while true
    

    [x, y, key] = ginput(1);
    if key==27
        break;
    end
    
    height = 501; width = 375;
    hold on;
    selectedPoint = plot(x,y,'or');
    legend(selectedPoint,'Selected point','Location','southwest');
    l_prime = F* [x;y;1];

    xlims = (-1000:1000) ;

    subplot(1,2,2);
    hold on

    eLine = plot(xlims,-(l_prime(3)+l_prime(1)*xlims)/l_prime(2));
    legend([selectedPoint, eLine],'Selected point','Epipolar Line','Location','southwest');
end

leftEpipole = null(F);
leftEpipole = leftEpipole/leftEpipole(3);

rightEpipole = null(F');
rightEpipole = rightEpipole/rightEpipole(3);

subplot(1,2,1); hold on;
ePole = plot(leftEpipole(1),leftEpipole(2),'xm','LineWidth',2);

subplot(1,2,2); hold on;
plot(rightEpipole(1),rightEpipole(2),'xm','LineWidth',2);
legend([selectedPoint, eLine, ePole],'Selected point','Epipolar Line','Epipole','Location','southwest');