function [F] = eightPointsAlgorithm(leftPoints,rightPoints)
% function Fundamental_Matrix =Eight_Point_Algorithm(x1,x2)
% Calculates the Fundamental matrix between two views from the normalized 8 point algorithm
% Inputs: 
%               x1      3xN     homogeneous coordinates of matched points in view 1
%               x2      3xN     homogeneous coordinates of matched points in view 2
% Outputs:
%               F       3x3     Fundamental matrix

% normalize the selected points
meanVecLeft = repmat(mean(leftPoints,2),1,size(leftPoints,2));
scaleLeft = 2/mean(sqrt(sum((leftPoints - meanVecLeft).^2,2)));

T_left = [  scaleLeft   0           -scaleLeft*meanVecLeft(1)
            0           scaleLeft   -scaleLeft*meanVecLeft(2)
            0           0           1   ];

meanVecRight = repmat(mean(rightPoints,2),1,size(rightPoints,2));
scaleRight = 2/mean(sqrt(sum((rightPoints - meanVecRight).^2,2)));

T_right = [ scaleRight  0           -scaleRight*meanVecRight(1)
            0           scaleRight  -scaleRight*meanVecRight(2)
            0           0           1   ];


normalizedLeft = T_left * leftPoints;
normalizedRight = T_right * rightPoints;

u = normalizedLeft(1,:)'; % u_l
u_prime = normalizedRight(1,:)'; % u_r

v = normalizedLeft(2,:)'; % v_l
v_prime = normalizedRight(2,:)'; % v_r

A = [u_prime.*u u_prime.*v u_prime v_prime.*u v_prime.*v v_prime u v ones(size(v,1),1)];

[U sigma V] = svd(A);
F = V(:,end);

% reshape F to a 3x3 matrix
F = reshape(F,[3 3])';

% Make sure that F is a rank 2 matrix (crop smallest eigenvalue)
[U sigma V] = svd(F);
sigma(3,3) = 0;
F = U * sigma * V';

% rescale it
F = T_right' * F * T_left;