clear all
clc

left = mean(double(imread('Matched Points/left.jpg')),3);
right = mean(double(imread('Matched Points/Right.jpg')),3);

useGroundTruth = true;

figure(1)
clf;
subplot(1,2,1);
imagesc(left);
colormap(gray) 
title('Left Image');

subplot(1,2,2);
imagesc(right);
colormap(gray)
title('Right Image');

A=load('Matched Points/Matched_Points.txt');
[M N] = size(A);


leftPoints = [A(:,3)'; A(:,4)'; ones(1,M)];
rightPoints = [A(:,1)'; A(:,2)'; ones(1,M)];

F = eightPointsAlgorithm(leftPoints,rightPoints);  

K = [-83.33333     0.00000   250.00000;
     0.00000   -83.33333   250.00000;
     0.00000     0.00000     1.00000];

E = K' * F * K;
disp('Essential matrix E:'); disp(E);

% Ground truth values
Rl = zeros(3);
tl = zeros(3,1);
Rr = zeros(3);
tr = zeros(3,1);

Rl = [  1.00000 0.00000 0.00000
        0.00000 0.92848 0.37139
        0.00000 -0.37139 0.92848 ];
Tl = [  0 -5 2
        5 0 0
        -2 0 0 ];
E_groundTruth = Rl*Tl;
disp('Essential matrix ground truth:'); disp(E_groundTruth);

% Use ground truth if boolean is true
if (useGroundTruth)
   E = E_groundTruth; 
end


[U sigma Vt] = svd(E);

W = [   0 -1 0;
        1 0 0;
        0 0 1];
    
R = U * W' * Vt;

t_crossProd = Vt'*W*sigma*Vt;
t = zeros(1,3);
t(1) = -t_crossProd(2,3);
t(2) = t_crossProd(1,3);
t(3) = t_crossProd(2,1);

% disp(t_crossProd*R);

% Reconstruct 3D points
r1 = R(1,:); r2 = R(2,:); r3 = R(3,:);

zCoord = zeros(1,length(rightPoints));
for i = 1:length(rightPoints)
    zCoord(i) = dot(r1 - rightPoints(1,i)*r3,t)/ dot(r1 - rightPoints(1,i)*r3,leftPoints(:,i));
end
left_xyz = [leftPoints(1:end-1,:); zCoord];

figure(2); hold on;
scatter3(left_xyz(1,:).*left_xyz(3,:), left_xyz(2,:).*left_xyz(3,:), left_xyz(3,:), '.')